const express = require('express')
const app     = express()
const socket  = require('socket.io')

app.get('/', (req, res) => {
	res.sendFile('index.html', { root: __dirname })
})

const server = app.listen(3000, () => {
	console.log('Listening on port 3000')
})

const io = socket(server)

io.on('connection', (socket) => {
	// event ketika ada pesan baru
	socket.on('new_message', (message) => {
		io.emit('new_message', message)
		console.log(message)
	})

	// ketika ada yang diskonek
	socket.on('disconnect', (message) => {
		console.log('User disconnected')
	})
})